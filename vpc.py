#!/usr/bin/env python
'''
Create VPC
'''
import boto3
from botocore.client import ClientError
import yaml
import json
import sys, os
from pprint import pprint

# Set a sane default that can be overridden by cli arg
CONFIG_DIR = '/home/scott/boto_scripts/vpc/config'


def connect_to_aws_service(service_name):
    session = boto3.setup_default_session(
        region_name=config.get('vpc').get('region'))
    return boto3.resource(service_name), boto3.client(service_name)


def connect_to_aws_ec2_api():
    session = boto3.setup_default_session(
        region_name=config.get('vpc').get('region'))
    return boto3.resource('ec2'), boto3.client('ec2')


def read_config():
    config_data = {}
    for f in os.listdir(CONFIG_DIR):
        with open(os.path.join(CONFIG_DIR, f)) as fp:
            config_data = yaml.safe_load(fp.read())

    pprint(config_data)
    return config_data


def ensure_key_pair_exists():
    key_name = '{}-{}-keypair'.format(
        config.get('vpc').get('name'), config.get('vpc').get('env'))

    try:
        ec2_client.describe_key_pairs(KeyNames=[key_name])
    except:
        key_pair_resp = ec2_client.create_key_pair(KeyName=key_name)
        return key_pair_resp.get('KeyMaterial')


def write_private_key_to_file(key_material):
    '''Write the new private key to file'''
    key_name = '{}-{}-keypair'.format(
        config.get('vpc').get('name'), config.get('vpc').get('env'))
    key_file = os.path.join('/home/scott/.ssh', key_name)

    with open(key_file, 'wb') as fp:
        fp.write(key_material)

    return key_file


def print_private_key_file(key_file):
    with open(key_file, 'r') as fp:
        print fp.read()


def name_tag(resource_id, name):
    ec2_resource.create_tags(Resources=[resource_id],
                             Tags=[{'Key': 'Name',
                                    'Value': name}])


def ensure_security_groups_exist():

    for group in config.get('security_groups'):

        group_name = '{}-{}-{}-sec-group'.format(
            group, config.get('vpc').get('name'), config.get('vpc').get('env'))

        print 'checking if sec group {} exists'.format(group_name)

        res = ec2_client.describe_security_groups(Filters=[
            {
                'Name': 'group-name',
                'Values': [group_name],
                'Name': 'vpc-id',
                'Values': [vpc_id]
            }
        ])

        for existing_groups in res.get('SecurityGroups'):

            if group_name == existing_groups.get('GroupName'):

                print '{} exists'.format(existing_groups.get('GroupName'))

                config['security_groups'][group][
                    'group_id'] = existing_groups.get('GroupId')
                break
        else:

            print 'creating : {}'.format(group_name)

            config['security_groups'][group]['group_id'] = create_sec_group(
                group_name)


def create_sec_group(name):

    security_group = ec2_resource.create_security_group(GroupName=name,
                                                        Description=name,
                                                        VpcId=vpc_id)

    name_tag(security_group._id, name)

    return security_group._id


def get_source_group_id(source_group_name):
    return config.get('security_groups').get(source_group_name).get('group_id')


def add_sec_group_authoizations():

    for group in config.get('security_groups'):

        for ingress in config.get('security_groups').get(group).get('ingress'):

            if 'IpRanges' in ingress:

                try:
                    ec2_client.authorize_security_group_ingress(
                        GroupId=config.get('security_groups').get(group).get(
                            'group_id'),
                        IpPermissions=[ingress])
                except:
                    print '{} exists'.format(ingress)

            elif 'UserIdGroupPairs' in ingress:

                for source_group in ingress['UserIdGroupPairs']:

                    source_group['GroupId'] = get_source_group_id(source_group[
                        'GroupId'])

                try:
                    ec2_client.authorize_security_group_ingress(
                        GroupId=config.get('security_groups').get(group).get(
                            'group_id'),
                        IpPermissions=[
                            ingress
                        ])
                except:
                    print '{} exists'.format(ingress)

            else:
                print 'Ingress needs to be either IpRanges or UserIdGroupPairs: {}'.format(
                    ingress)


def ensure_vpc_exists():
    vpc_name = '{}-{}-vpc'.format(
        config.get('vpc').get('name'), config.get('vpc').get('env'))
    filters = [{'Name': 'tag-value', 'Values': [vpc_name]}]
    vpcs = ec2_client.describe_vpcs(Filters=filters)
    vpc_resource = None

    for vpc in vpcs.get('Vpcs'):

        for tag in vpc.get('Tags'):

            if tag.get('Value') == vpc_name:

                print '{} exists'.format(tag.get('Value'))

                vpc_resource = ec2_resource.Vpc(vpc.get('VpcId'))
                break

    if not vpc_resource:

        print 'creating {}'.format(vpc_name)

        vpc_resource = ec2_resource.create_vpc(
            CidrBlock=config.get('vpc').get('cidr_block'))

        name_tag(vpc_resource._id, vpc_name)

    ensure_subnets_exist(vpc_resource)

    ensure_internet_gateway_exists(vpc_resource)

    config['vpc']['vpc_id'] = vpc_resource._id

    return vpc_resource._id


def ensure_subnets_exist(vpc_resource):

    if config.get('vpc').get('subnets'):

        for name, info in config.get('vpc').get('subnets').iteritems():

            subnet_name = '{}-{}-{}-subnet'.format(
                config.get('vpc').get('name'), config.get('vpc').get('env'),
                name)

            if subnet_name not in [tag.get('Value')
                                   for subnet in vpc_resource.subnets.all()
                                   for tag in subnet.tags]:

                print 'creating subnet {} {}'.format(subnet_name, info)

                create_subnet_res = vpc_resource.create_subnet(
                    CidrBlock=info.get('cidr'),
                    AvailabilityZone='{}{}'.format(
                        config.get('vpc').get('region'), config.get('vpc').get(
                            'az')))

                config['vpc']['subnets'][name][
                    'subnet_id'] = create_subnet_res._id

                subnet_name = '{}-{}-{}-subnet'.format(
                    config.get('vpc').get('name'),
                    config.get('vpc').get('env'), name)

                name_tag(create_subnet_res._id, subnet_name)

            else:

                subnet_id = [subnet._id
                             for subnet in vpc_resource.subnets.all()
                             for tag in subnet.tags
                             if subnet_name == tag.get('Value')]

                config['vpc']['subnets'][name]['subnet_id'] = subnet_id[0]


def ensure_routes_exist():
    for name, info in config.get('vpc').get('subnets'):

        if name == 'private':

            vpc_resource = ec2_resource.Vpc(config.get('vpc').get('vpc_id'))
            route_table_res = vpc_resource.create_route_table()
            route_table_resource = ec2_resource.RouteTable(route_table_res._id)
            route_table_resource.associate_with_subnet(config.get('vpc').get(
                'subnets').get(name).get('subnet_id'))
            create_route_res = route_table_resource.create_route(
                DestinationCidrBlock=config.get('vpc').get('subnets').get(
                    name).get('cidr'),
                InstanceId=config.get('vpc').get('nat_instance').get(
                    'instance_id'))


def ensure_internet_gateway_exists(vpc_resource):

    if config.get('vpc').get('internet_gateway'):

        igw_name = '{}-{}-igw'.format(
            config.get('vpc').get('name'),
            config.get('vpc').get('env'), )

        if igw_name not in [tag.get('Value')
                            for igw in vpc_resource.internet_gateways.all()
                            for tag in igw.tags]:

            create_res = ec2_resource.create_internet_gateway()

            name_tag(create_res._id, igw_name)

            attach_res = vpc_resource.attach_internet_gateway(
                InternetGatewayId=create_res._id)


def ensure_load_balancer_exists():
    '''Ensure and external, internet facing ELB exists'''

    elb_client = boto3.client('elb')

    elb_name = '{}-{}-elb'.format(
        config.get('vpc').get('name'), config.get('vpc').get('env'))

    try:
        elb_client.describe_load_balancers(LoadBalancerNames=[elb_name])

        print 'ELB {} exists'.format(elb_name)

    except:

        print 'Creating ELB {}'.format(elb_name)

        responce = elb_client.create_load_balancer(
            LoadBalancerName=elb_name,
            Listeners=[
                {
                    'Protocol': 'http',
                    'LoadBalancerPort': 80,
                    'InstanceProtocol': 'http',
                    'InstancePort': 80
                }
            ],
            Subnets=[config.get('vpc').get('subnets').get('public').get(
                'subnet_id')],
            SecurityGroups=[config.get('security_groups').get('elb').get(
                'group_id')],
            Tags=[
                {
                    'Key': 'Name',
                    'Value': elb_name
                }
            ])


def ensure_nat_instance_exists(vpc_id):
    '''Launch a NAT instance'''
    nat_instance_id = None
    nat_name = '{}-{}-nat-instance'.format(
        config.get('vpc').get('name'), config.get('vpc').get('env'))

    reservations = ec2_client.describe_instances(
        Filters=[{'Name': 'tag:Name',
                  'Values': [nat_name]}])

    for reservation in reservations.get('Reservations'):

        for instance in reservation.get('Instances'):

            if nat_name in [
                    i.get('Value') for i in instance.get('Tags')
            ] and instance.get('State').get(
                    'Name') == 'running' and instance.get('VpcId') == vpc_id:

                print 'Found running nat instance'
                nat_instance_id = instance.get('InstanceId')
                config['vpc']['nat_instance']['instance_id'] = nat_instance_id

    if not nat_instance_id:
        print 'Launching nat instance'
        key_name = '{}-{}-keypair'.format(
            config.get('vpc').get('name'), config.get('vpc').get('env'))

        vpc_resource = ec2_resource.Vpc(vpc_id)

        security_group_id = config.get('security_groups').get('nat').get(
            'group_id')
        subnet_id = config.get('vpc').get('subnets').get('public').get(
            'subnet_id')

        instance_res = ec2_resource.create_instances(
            MaxCount=1,
            MinCount=1,
            ImageId=config.get('vpc').get('nat_instance').get('ami_id'),
            KeyName=key_name,
            SecurityGroupIds=[security_group_id],
            InstanceType=config.get('vpc').get('nat_instance').get('type'),
            SubnetId=subnet_id)

        for instance in instance_res:

            name_tag(instance._id, nat_name)

            config['vpc']['nat_instance']['instance_id'] = instance._id


def ensure_s3_buckets_exist():

    s3_resource, s3_client = connect_to_aws_service('s3')

    for name in config.get('s3_buckets'):
        bucket_name = '{}-{}-{}'.format(
            config.get('vpc').get('name'), config.get('vpc').get('env'), name)

        try:
            s3_client.head_bucket(Bucket=bucket_name)
        except ClientError:
            print 'Creating {} bucket'.format(bucket_name)

            bucket = s3_resource.create_bucket(
                ACL='private',
                Bucket=bucket_name,
                CreateBucketConfiguration={
                    'LocationConstraint': config.get('vpc').get('region')
                })


def ensure_iam_roles_exist():
    iam_resource, iam_client = connect_to_aws_service('iam')

    iam_resource = boto3.resource('iam',
                                  region_name=config.get('vpc').get('region'))

    policy_doc = json.dumps({"Version": "2012-10-17",
                             "Statement": {"Effect": "Allow",
                                           "Principal": {"Service":
                                                         "ec2.amazonaws.com"},
                                           "Action": "sts:AssumeRole"}})

    for role in config.get('iam_roles'):
        policy = json.dumps(role.get('policy'))
        create_policy_resp = None

        try:
            iam_resource.create_role(RoleName=role.get('name'),
                                     AssumeRolePolicyDocument=policy_doc)
        except:
            print 'role {} exists'.format(role.get('name'))

        try:
            create_policy_resp = iam_client.create_policy(
                PolicyName=role.get('name'),
                PolicyDocument=policy,
                Description=role.get('description'))
        except:
            print 'policy {} exists'.format(role.get('name'))

        if create_policy_resp:
            policy_arn = create_policy_resp.get('Policy').get('Arn')

            iam_client.attach_role_policy(
                RoleName=role.get('name'),
                PolicyArn=create_policy_resp.get('Policy').get('Arn'))


config = read_config()

ec2_resource, ec2_client = connect_to_aws_service('ec2')

ensure_s3_buckets_exist()

ensure_iam_roles_exist()

vpc_id = ensure_vpc_exists()

ensure_security_groups_exist()

add_sec_group_authoizations()

key_material = ensure_key_pair_exists()

if key_material:
    key_file = write_private_key_to_file(key_material)

ensure_nat_instance_exists(vpc_id)

ensure_load_balancer_exists()

print '\nConfig: \n'
pprint(config)
